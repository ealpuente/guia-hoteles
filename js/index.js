$(function () {
    $('[data-toggle="popover"]').popover()
    $('[data-toggle="tooltip"]').tooltip()
    $('.carousel').carousel({
      interval: 200
    })
})

$('#masinfo').on('show.bs.modal', function(e) {
  console.log('Mostrando modal');

  $('.btn-masinfo').addClass('disabled')
  $('.btn-masinfo').removeClass('btn-secondary')
  $('.btn-masinfo').addClass('btn-outline-secondary');

});

$('#masinfo').on('shown.bs.modal', function(e) {
  console.log('Modal mostrada');
});

$('#masinfo').on('hide.bs.modal', function(e) {
  console.log('Ocultando modal');

  $('.btn-masinfo').removeClass('btn-outline-secondary');
  $('.btn-masinfo').addClass('btn-secondary')
  $('.btn-masinfo').removeClass('disabled')

});

$('#masinfo').on('hidden.bs.modal', function(e) {
  console.log('Modal Ocultada');
});